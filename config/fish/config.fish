if status is-interactive
    # Commands to run in interactive sessions can go here
end

set fish_greeting
alias imgcat "kitty +kitten icat"
alias clipboard "kitty +kitten clipboard"
alias ssh "kitty +kitten ssh"


